#!/usr/bin/ruby

require 'gtk3'
require 'ruby-mpd'
load    'album_art.rb'

class RubyApp < Gtk::Window
    def initialize
        super
        @threads = []
        @album_art_handler = AlbumArtHandler.new
        @mpd = MPD.new 'localhost', 6600, { :callbacks => true }
        @mpd.connect
        init_ui
    end
    
    def init_ui
        signal_connect "destroy" do 
            @mpd.disconnect
            @threads.each { |thread| thread.join }
            Gtk.main_quit 
        end        
        @main_split = Gtk::Paned.new :vertical
        @controls   = Gtk::Box.new :horizontal
        #@scrolled_window = Gtk::ScrolledWindow.new 
        @grid       = Gtk::Grid.new 
        #@scrolled_window.add @grid
        @grid.set_column_spacing 5
        set_window_position(:center)
        set_default_size 600, 400
        set_title "Music Player"
        add @main_split
        @main_split.add2 @grid
        show_albums
        show_all
        @main_split.add  @controls
        @mpd.on :song do |songid|
            response = @mpd.current_song
            album    = response.album
            artist   = response.artist
            if not @album_art_handler.album_art_downloaded?(album, artist)
                @album_art_handler.download_album_art(album, artist)
            end
        end
    end

    def show_albums
        albums = get_albums
        column = 0
        row    = 0
        albums.each do |album|
            if album.length == 0 
                next 
            end
            name_album = album[:name_album]
            artist     = album[:artist]
            box       = Gtk::Box.new :vertical
            album_art = Gtk::Box.new :vertical
            if @album_art_handler.album_art_downloaded? name_album, artist
                pixbuf  = @album_art_handler.get_image_by_album_artist(name_album, artist)
            else 
                pixbuf = Gdk::Pixbuf.new :file   => 'missing.png'
            end
            pixbuf  = pixbuf.scale(150, 150, Gdk::Pixbuf::INTERP_BILINEAR)
            album_art  = Gtk::Image.new  :pixbuf => pixbuf 
            box.add album_art
            box.add Gtk::Label.new name_album
            @grid.attach box, column, row, 1, 1
            if column == 2
                column = 0
                row    = row + 1
            else
                column = column + 1
            end
        end
    end

    def draw_list_and_album_art
        draw_album_art_container
        @artist_list = get_artist_list
        @scrolled_artist = Gtk::ScrolledWindow.new
        @scrolled_artist.add @artist_list
        @scrolled_artist.set_hexpand true
        @scrolled_artist.set_vexpand true
        @album_art = Gtk::Box.new :vertical
        @album_art.add @album_art_container
        @grid.attach @album_art             , 0, 0, 3, 3
        @grid.attach @scrolled_artist       , 3, 0, 1, 1
        show_all
        draw_controls
        if @mpd.playing?
            show_album_art
        end
    end

    def draw_album_art_container
        #Album container
        @album_art_container = Gtk::Box.new :horizontal
        @album_art_container.set_name 'window'
    end

    def show_album_art
        @threads << Thread.new {
            response = @mpd.current_song
            album    = response.album
            artist   = response.artist
            if not @album_art_handler.album_art_downloaded?(album, artist)
                @album_art_handler.download_album_art(album, artist)
            end
            @album_art_container.children.each do |children|
                @album_art_container.remove(children) 
            end

            pixbuf = @album_art_handler.get_image_by_album_artist(album, artist)
            if not pixbuf
                pixbuf = Gdk::Pixbuf.new :file   => 'missing.png'
            end
            puts pixbuf
            pixbuf    = pixbuf.scale(150, 150, Gdk::Pixbuf::INTERP_BILINEAR)
            album_art = Gtk::Image.new  :pixbuf => pixbuf
            @album_art_container.add album_art
            @album_art_container.show_all
        }
    end

    def draw_controls
        #Next button
        @next_button = Gtk::Button.new :stock_id => Gtk::Stock::MEDIA_NEXT
        @next_button.signal_connect 'clicked' do
            @mpd.send_command 'next'
        end
        #Pause button
        @pause_button = Gtk::Button.new :stock_id => Gtk::Stock::MEDIA_PAUSE
        @pause_button.signal_connect 'clicked' do
            @mpd.send_command 'pause'
            @play_button.show
            @pause_button.hide
            @play_button.grab_focus
        end
        #Play button
        @play_button = Gtk::Button.new :stock_id => Gtk::Stock::MEDIA_PLAY
        @play_button.signal_connect 'clicked' do
            @mpd.play
            @play_button.hide
            @pause_button.show
            @pause_button.grab_focus
        end
        @controls.add @next_button
        @controls.add @play_button
        @controls.add @pause_button
        @controls.show_all
        @pause_button.hide if @mpd.stopped? or @mpd.paused?
        @play_button.hide if not @mpd.stopped? and not @mpd.paused?
    end

    def get_artist_list
        artist_tv    = Gtk::TreeView.new
        artist_model = Gtk::ListStore.new(String)

        #columns
        artist_name = Gtk::TreeViewColumn.new('Album', Gtk::CellRendererText.new, :text => 0)
        artist_tv.append_column artist_name
        artist_tv.set_model artist_model

        #add artist to the list
        albums = get_albums
        #albums = []
        albums.each do |album|
            iter = artist_model.append()
            iter.set_value(0, album[:name_album])
        end
        artist_tv
        artist_tv.show_all
    end

    def get_albums
        albums  = []
        artists = @mpd.send_command 'list', 'artist'
        artists.each do |artist| 
            album = @mpd.send_command 'list', 'album', 'artist', artist
            if album.kind_of? String
                albums << { :name_album => album, :artist => artist }
            else
                if album.kind_of? Array
                    album.each do |a|
                        albums << { :name_album => a, :artist => artist }
                    end
                end
            end
        end
        albums
    end

end

Gtk.init
    window = RubyApp.new
Gtk.main
