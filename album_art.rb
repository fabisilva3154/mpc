require 'spotify-client'
require 'net/http'
require 'nokogiri'
require 'digest/md5'

class AlbumArtHandler

	def get_filename_by_album_artist(album, artist)
		Digest::MD5.hexdigest("#{artist}#{album}")
	end

	def download_image(url, filename)
		`wget -nv --output-document "#{filename}" #{url} `
	end

	def get_image_by_album_artist(album, artist)
		filename = get_filename_by_album_artist(album, artist)
        return false if not File.exist?(filename)
		Gdk::Pixbuf.new :file   => filename
	end

	def album_art_downloaded?(album, artist)
		filename = get_filename_by_album_artist(album, artist)
        File.exist?(filename)
	end

    def search_spotify(album, artist)
        client  = Spotify::Client.new
        results = client.search('album', album+' '+artist)
        albums  = results['albums']['items']
        url     = false
        albums.each do |album|
            album['images'].each do |image|
                if not url
                    url = image['url']
                end
            end
        end
        url
    end

	def search_album_exchange(album, artist)
        uri       = URI('http://www.albumartexchange.com/covers.php')
        params    = { :sort => 1, :q => "#{album} #{artist}", :fltr => 1, :page => '', :bgc => '' }
        uri.query = URI.encode_www_form(params)
        res       = Net::HTTP.get_response(uri)
        xml_doc   = Nokogiri::XML(res.body)
        puts xml_doc
        links     = xml_doc.css("td a")
        puts links
	end

    def search_musicbrainz(album, artist)
        release   = "album:#{album} AND artist:#{artist}"
        uri       = URI('http://musicbrainz.org/ws/2/release/')
        params    = { :query => release }
        uri.query = URI.encode_www_form(params)
        res       = Net::HTTP.get_response(uri)
        xml_doc   = Nokogiri::XML(res.body)
        i = 0
        image_found = false
        cover_response = ''
        release_count = xml_doc.css("release-list").first.attr('count').to_i
        search_spotify(album, artist)
        puts 'seeking album art'
        while not image_found and i < release_count
            release_item = xml_doc.css("release").at(i)
            if release_item #and release_item.css("title").text.uppercase == album_name.uppercase
                id = release_item.attr('id')
                puts id
                cover_response = Net::HTTP.get('coverartarchive.org', '/release/'+id+'/front')
                if cover_response.match('See:')
                    image_found = true
                end
            end
            i = i + 1
        end
        cover_response.partition(": ")[2]
    end

    def get_url_album_art(album, artist)
        search_spotify(album, artist)
    end

    def download_album_art(album, artist)
    	url = get_url_album_art(album, artist)
    	if url.length > 0
    		filename = get_filename_by_album_artist(album, artist)
    		download_image(url, filename)
    	end
    	url.length > 0
    end
end
